.model small
.code
org 100h

mulai:
	jmp proses
	kal_a db 'Apabila jari jari suatu bola = 3cm, maka berapakah volume bola tersebut ?    $'	
	kal_b db 10,13,'             4    $'
	kal_c db 10,13,'  V bola =  --- phi * 3 * 3 * 3   = 36 cm^3 $'
	kal_d db 10,13,'             3    $'
	
	proses:
	
	mov ah, 09h
	lea dx, kal_a   ; tampilkan kal_a
	int 21h
	
	mov ah, 09h
	lea dx, kal_b    ; tampilkan kal_b
	int 21h
	
	mov ah, 09h
	lea dx, kal_c   ; tampilkan kal_c
	int 21h

	mov ah, 09h
	lea dx, kal_d   ; tampilkan kal_d
	int 21h
	
	int 20h
end mulai